## Youtube tutorial

From [here](https://www.youtube.com/playlist?list=PLSfH3ojgWsQosqpQUc28yP9jJZXrEylJY).

UPD [here](https://www.youtube.com/watch?v=_zNi37BJVBk&list=PPSV).

## Stack

Laravel Framework 10.x-dev

PHP 8.2.0

## License

Licensed under the [MIT license](https://opensource.org/licenses/MIT).
